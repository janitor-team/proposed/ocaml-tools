Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: ocaml-autoconf/*
Source: http://ocaml-autoconf.forge.ocamlcore.org/
Copyright: © 2009      Richard W.M. Jones
           © 2009      Stefano Zacchiroli
           © 2000-2005 Olivier Andrieu
           © 2000-2005 Jean-Christophe Filliâtre
           © 2000-2005 Georges Mariano
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. Neither the name of the University nor the names of its contributors
     may be used to endorse or promote products derived from this software
     without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

Files: autoconf/*
Source: http://www.lri.fr/~filliatr/software.en.html
Upstream-Contact: Jean-Christophe Filliâtre <filliatr@lri.fr>
Copyright: © 2001 Jean-Christophe Filliâtre
License: LGPL-2
 This program is distributed under the GNU Library General Public
 License version 2.  On a Debian system the GNU LGPL can be found in
 the file /usr/share/common-licenses/LGPL-2 .

Files: omlet-0.13/*
Source: http://www.vim.org/scripts/script.php?script_id=1196
Upstream-Contact: David Baelde <david.baelde@ens-lyon.org>
Copyright: (C) 2005 David Baelde
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian systems you can find a copy of this license in
 /usr/share/common-licenses/GPL-2

Files: ocamldot/*
Source: http://www.research.att.com/~trevor/ocamldot/index.html
Upstream-Contact: Trevor Jim <trevor@research.att.com>
Copyright: disclaimed by the author
License: public-domain
 Ocamldot was written by Trevor Jim.  It is in the public domain; use
 it however you like, at your own risk.

